'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Blog extends Model {
    static associate(models) {
      Blog.belongsTo(models.User,{foreignKey:'iPostedBy'});
      Blog.hasOne(models.Like, {foreignKey:'iBlogId'});
      Blog.hasMany(models.Comment, {foreignKey:'iBlogId'});
      Blog.hasMany(models.Image, {foreignKey:'iBlogId'});
    }
  }
  Blog.init({
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    sTitle: {
      allowNull: false,
      type: DataTypes.STRING(25),
      unique:true,
      validate:{
        notEmpty: true
      }
    },
    sDescription: {
      allowNull: false,
      type: DataTypes.STRING,
      validate:{
        notEmpty: true
      }
    },
    iPostedBy: {
      allowNull: false,
      type: DataTypes.INTEGER,
      references: {
        model: 'Users',
        key: 'id',
      },
    },
    bIsPosted: {
      type: DataTypes.BOOLEAN,
      defaultValue: true
    },
    dPublishedDate: {
      allowNull: false,
      type: DataTypes.DATE,
      validate:{
        isDate: true, 
        notEmpty:true
      }
    }
  }, {
    sequelize,
    modelName: 'Blog',
    indexes:{
      name:'blog',
      fields:['sTitle']
    },
    timestamps:true,
    createdAt:'dCreatedAt',
    updatedAt:'dUpdatedAt',
  });
  return Blog;
};