'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Image extends Model {
    static associate(models) {
      Image.belongsTo(models.Blog,{foreignKey:'iBlogId'});
    }
  }
  Image.init({
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    iBlogId: {
      allowNull: false,
      type: DataTypes.INTEGER,
      references: {
        model: 'Blogs',
        key: 'id',
      },
    },
    sImage: {
      allowNull: false,
      type: DataTypes.STRING
    }
  }, {
    sequelize,
    modelName: 'Image',
    timestamps:true,
    createdAt:'dCreatedAt',
    updatedAt:'dUpdatedAt',
  });
  return Image;
};