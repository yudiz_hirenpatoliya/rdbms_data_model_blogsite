'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class User extends Model {
    static associate(models) {
      User.hasMany(models.Blog,{foreignKey:'id'});
      User.hasOne(models.Like,{foreignKey:'iUserId'});
      User.hasMany(models.Comment,{foreignKey:'iUserId'});
    }
  }
  User.init({
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    sUserName: {
      allowNull: false,
      type: DataTypes.STRING(25),
      unique: true,
      validate:{
        notEmpty: true
      }
    },
    sEmail: {
      allowNull: false,
      type: DataTypes.STRING(25),
      unique: true,
      validate:{
        isEmail: true,
        notEmpty: true
      },
    },
    sPasswword: {
      allowNull: false,
      type: DataTypes.STRING(50),
      validate:{
        notEmpty: true
      }
    },
    sMobile: {
      allowNull: false,
      type: DataTypes.STRING(10),
      unique: true,
      validate:{
        notEmpty: true
      }
    },
    sRole: {
      type: DataTypes.STRING,
      defaultValue: 'USER',
      validate:{
        isIn:[['USER','ADMIN']],
      }
    }
  }, {
    sequelize,
    modelName: 'User',
    indexes:{
      name:'user_sUserName_sEmail',
      fields:['sUserName','sEmail']
    },  
    timestamps:true,
    createdAt:'dCreatedAt',
    updatedAt:'dUpdatedAt',
  });
  return User;
};