'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Like extends Model {
    static associate(models) {
      Like.belongsTo(models.Blog,{foreignKey:'iBlogId'});
      Like.belongsTo(models.User,{foreignKey:'iUserId'});
    }
  }
  Like.init({
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    iBlogId: {
      allowNull: false,
      type: DataTypes.INTEGER,
      references: {
        model: 'Blogs',
        key: 'id',
      },
    },
    iUserId: {
      allowNull: false,
      type: DataTypes.INTEGER,
      references: {
        model: 'Users',
        key: 'id',
      },
    }
  }, {
    sequelize,
    modelName: 'Like',
    timestamps:true,
    createdAt:'dCreatedAt',
    updatedAt:'dUpdatedAt',
  });
  return Like;
};