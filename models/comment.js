'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Comment extends Model {
    static associate(models) {
      Comment.belongsTo(models.Blog,{foreignKey:'iBlogId'});
      Comment.belongsTo(models.User,{foreignKey:'iUserId'});
    }
  }
  Comment.init({
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    iBlogId: {
      allowNull: false,
      type: DataTypes.INTEGER,
      references: {
        model: 'Blogs',
        key: 'id',
      },
    },
    iUserId: {
      allowNull: false,
      type: DataTypes.INTEGER,
      references: {
        model: 'Users',
        key: 'id',
      },
    },
    sDescription: {
      allowNull: false,
      type: DataTypes.STRING,
    },
  }, {
    sequelize,
    modelName: 'Comment',
    timestamps:true,
    createdAt:'dCreatedAt',
    updatedAt:'dUpdatedAt',
  });
  return Comment;
};