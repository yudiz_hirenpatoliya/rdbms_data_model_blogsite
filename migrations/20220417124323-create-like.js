'use strict';
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('Likes', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      iBlogId: {
        allowNull: false,
				type: Sequelize.INTEGER,
				references: {
					model: 'Blogs',
					key: 'id',
				},
      },
      iUserId: {
        allowNull: false,
				type: Sequelize.INTEGER,
				references: {
					model: 'Users',
					key: 'id',
				},
      },
      dCreatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      dUpdatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  async down(queryInterface) {
    await queryInterface.dropTable('Likes');
  }
};