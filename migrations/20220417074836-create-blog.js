'use strict';
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('Blogs', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      sTitle: {
        allowNull: false,
        type: Sequelize.STRING(25),
        unique:true,
        validate:{
          notEmpty: true
        }
      },
      sDescription: {
        allowNull: false,
        type: Sequelize.STRING,
        validate:{
          notEmpty: true
        }
      },
      iPostedBy: {
        allowNull: false,
				type: Sequelize.INTEGER,
				references: {
					model: 'Users',
					key: 'id',
				},
      },
      bIsPosted: {
        type: Sequelize.BOOLEAN,
        defaultValue: true,
      },
      dPublishedDate: {
        allowNull: false,
        type: Sequelize.DATE,
        validate:{
          isDate: true, 
          notEmpty: true
        }
      },
      dCreatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      dUpdatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  async down(queryInterface) {
    await queryInterface.dropTable('Blogs');
  }
};