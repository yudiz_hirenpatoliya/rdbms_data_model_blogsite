'use strict';
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('Images', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      iBlogId: {
        allowNull: false,
				type: Sequelize.INTEGER,
				references: {
					model: 'Blogs',
					key: 'id',
				},
      },
      sImage: {
        allowNull: false,
        type: Sequelize.STRING
      },
      dCreatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      dUpdatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  async down(queryInterface) {
    await queryInterface.dropTable('Images');
  }
};