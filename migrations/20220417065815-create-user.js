'use strict';
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('Users', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER
      },
      sUserName: {
        allowNull: false,
        type: DataTypes.STRING(25),
        unique: true,
        validate:{
          notEmpty: true
        }
      },
      sEmail: {
        allowNull: false,
        type: DataTypes.STRING(25),
        unique: true,
        validate:{
          isEmail: true,
          notEmpty: true
        },
      },
      sPasswword: {
        allowNull: false,
        type: DataTypes.STRING(50),
        validate:{
          notEmpty: true
        }
      },
      sMobile: {
        allowNull: false,
        type: DataTypes.STRING(10),
        unique: true,
        validate:{
          notEmpty: true
        }
      },
      sRole: {
        type: DataTypes.STRING,
        defaultValue: 'USER',
        validate:{
          isIn:[['USER','ADMIN']],
        }
      },  
      dCreatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      dUpdatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
    await queryInterface.addIndex({ tableName: 'Users' }, ['sUserName']);
  },
  async down(queryInterface) {
    await queryInterface.dropTable('Users');
  }
};