const { Sequelize } = require("sequelize");

const sequelize = new Sequelize('BlogPost', 'root', 'sql2022', {
    host: 'localhost',
    dialect: 'mysql'
});

try {
    sequelize.authenticate();
    console.log('connection successful')
} catch (error) {
    console.log('connection abort dur to ' + error.message)
}

module.exports = Sequelize;